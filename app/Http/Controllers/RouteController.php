<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Route;

class RouteController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index()
  {
    $routes = Route::all();

    return view('/routes/index', compact('routes'));
  }

  public function create()
  {
      return view('routes.create');
  }

  public function store(Request $request)
  {
      $data = $this->validate($request,[

      'from_point' => ['required'],
      'to_point' => ['required'],
      'time' => ['required'],
      'cost' => ['required'],

      ]);


      $route = Route::create([
          'from_point' => $data['from_point'],
          'to_point' => $data['to_point'],
          'time' => $data['time'],
          'cost' => $data['cost'],
      ]);


      $route->save();

      return redirect('/routes');

  }

  public function destroy(Route $route)
 {

     $route->delete();

     return redirect('/routes');
 }

 public function edit(Route $route)
 {
     return view('/routes/edit', compact('route'));
 }

 public function update(Request $request, $id)
 {
     $data = request()->validate([
       'from_point' => ['required'],
       'to_point' => ['required'],
       'time' => ['required'],
       'cost' => ['required'],
     ]);

     $route = Route::find($id);
     $route->from_point = $request->get('from_point');
     $route->to_point = $request->get('to_point');
     $route->time = $request->get('time');
     $route->cost = $request->get('cost');

     $route->save();

     return redirect('/routes');
 }

}
