<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Users
Route::get('/users', 'UserController@index')->name('users');

//Routes
Route::get('/routes', 'RouteController@index')->name('routes');
Route::get('/routes/create', 'RouteController@create')->name('routes.create');
Route::post('/routes', 'RouteController@store')->name('routes.store');
Route::delete('/routes/{route}', 'RouteController@destroy')->name('routes.destroy');
Route::get('/routes/{route}/edit', 'RouteController@edit')->name('routes.edit');
Route::patch('/routes/{route}', 'RouteController@update')->name('routes.update');
