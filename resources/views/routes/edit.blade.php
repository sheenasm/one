@extends('layouts/app')


@section('content')

<div class="container">
	<h1>Edit Route</h1>

</div>

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
  			<div class="card">
   		 		<div class="card-body">
        			<form method="post" action="{{ route('routes.update', $route) }}">
                        @method('PATCH')


                        <div class="form-group row">
                            <label for="from_point" class="col-md-4 col-form-label text-md-right">From Point</label>

                            <div class="col-md-6">
                                <input id="from_point" type="text" class="form-control @error('from_point') is-invalid @enderror" name="from_point" value="{{ $route->from_point }}" required autocomplete="from_point" autofocus>

                                @error('from_point')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="to_point" class="col-md-4 col-form-label text-md-right">To Point</label>

                            <div class="col-md-6">
                                <input id="to_point" type="text" class="form-control @error('to_point') is-invalid @enderror" name="to_point" value="{{ $route->to_point }}" required autocomplete="to_point">

                                @error('to_point')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="time" class="col-md-4 col-form-label text-md-right">Time</label>

                            <div class="col-md-6">
                                <input id="time" type="text" class="form-control @error('time') is-invalid @enderror" name="time" value="{{ $route->time }}" required autocomplete="time">

                                @error('time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cost" class="col-md-4 col-form-label text-md-right">Cost</label>

                            <div class="col-md-6">
                                <input id="cost" type="text" class="form-control @error('cost') is-invalid @enderror" name="cost" value="{{ $route->cost }}" required autocomplete="cost">

                                @error('cost')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                            @csrf
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
		   		 </div>
		  	</div>
		 </div>
	</div>
</div>

@endsection
