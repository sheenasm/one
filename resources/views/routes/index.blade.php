@extends('layouts/app')


@section('title', 'Users')


@section('content')

<div class="container">
<h1>Routes</h1>
</div>

<div class="container mb-3">
  <a href="{{ route('routes.create') }}" class="btn btn-primary">ADD Route</a>
</div>

<div class="container">
  <div class="card">
    <div class="card-body">

        <table class="table table-striped text-center">
          <thead>
            <tr>
              <th width="20%">From Point</th>
              <th width="20%">To Point</th>
              <th width="20%">Time</th>
              <th width="15%">Cost</th>
              <th width="20%">Action</th>
            </tr>
          </thead>
         @forelse($routes as $route)
            <tr>
              <td>{{ $route->from_point }}</td>
              <td>{{ $route->to_point }}</td>
              <td>{{ $route->time }} days</td>
              <td>{{ $route->cost }}</td>
              <td>
                <a href="{{ route('routes.edit', $route->id)}}"><button type="button" class="btn btn-primary float-left mr-3">Edit</button></a>
                <form action="{{ route('routes.destroy', $route->id)}}" method="post" class="float-left">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
              </td>
              @csrf
             @empty
              <p>No data</p>
            </tr>
          @endforelse
      </table>
    </div>
  </div>
</div>


@endsection
