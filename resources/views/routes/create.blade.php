@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add Route') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('routes.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="from_point" class="col-md-4 col-form-label text-md-right">{{ __('From Point') }}</label>

                            <div class="col-md-6">
                                <input id="from_point" type="text" class="form-control @error('from_point') is-invalid @enderror" name="from_point" value="{{ old('from_point') }}" required autocomplete="from_point" autofocus>

                                @error('from_point')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="to_point" class="col-md-4 col-form-label text-md-right">{{ __('To Point') }}</label>

                            <div class="col-md-6">
                                <input id="to_point" type="text" class="form-control @error('to_point') is-invalid @enderror" name="to_point" value="{{ old('to_point') }}" required autocomplete="to_point" autofocus>

                                @error('to_point')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>

                            <div class="col-md-6">
                                <input id="time" type="text" class="form-control @error('time') is-invalid @enderror" name="time" value="{{ old('time') }}" required autocomplete="time" autofocus>

                                @error('time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cost" class="col-md-4 col-form-label text-md-right">{{ __('Cost') }}</label>

                            <div class="col-md-6">
                                <input id="cost" type="text" class="form-control @error('cost') is-invalid @enderror" name="cost" value="{{ old('cost') }}" required autocomplete="cost" autofocus>

                                @error('cost')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <p id="status" type="text" name="status" class="hidden"> </p>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('ADD') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
