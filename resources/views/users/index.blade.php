@extends('layouts/app')


@section('title', 'Users')


@section('content')

<div class="container">
<h1>Users</h1>
</div>

<div class="container">
  <div class="card">
    <div class="card-body">

        <table class="table table-striped text-center">
          <thead>
            <tr>
              <th width="20%">User Name</th>
              <th width="20%">Email</th>
              <th width="15%">User Type</th>
              <th width="20%">Action</th>
            </tr>
          </thead>
         @forelse($users as $user)
            <tr>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->user_type }}</td>
              <td>
              </td>
              @csrf
             @empty
              <p>No data</p>
            </tr>
          @endforelse
      </table>
    </div>
  </div>
</div>


@endsection
